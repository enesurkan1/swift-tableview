//
//  ViewController.swift
//  Deneme1
//
//  Created by Enes urkan on 11.10.2017.
//  Copyright © 2017 Enes urkan. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {

    
    var products = ["Tv Ünitesi","Tv","Masa","Sandalye","Koltuk","ev","yaprak","sehpa"]
    var prices = ["1232TL","2134TL","32432TL","21313TL","21321TL","22TL","33TL","55TL"]
    var stock = ["4","1","2","3","22","3","22","11"]
    var image = ["profil","profil","profil","profil","profil","profil","profil","profil","raf"]
    
    
    override func viewDidLoad() {  //kurucu fonk
        super.viewDidLoad()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //table view satır sayısı
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return products.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! ProductTableViewCell // hangisınıfın  özelleiklerine göre çalısacaksa onu as olarak koyuyoruz yanına
        
    
            cell.productName.text = products[indexPath.row]
            cell.productPrice.text = stock  [indexPath.row]
            cell.productValue.text = prices [indexPath.row]
            cell.remainTime.text = "3 Saat"
            cell.productImage.image = UIImage(named : "profil")
       
        
        
     
        
        
        return cell
    }
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }

}

